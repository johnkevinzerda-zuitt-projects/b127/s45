import {Button, Card} from 'react-bootstrap';

export default function CourseCard () {
	return (
	<Card>
		<Card.Body>
			<Card.Title><h3>Machine Learning</h3></Card.Title>	
			<h5>Description</h5>
			<p>
				Machine learning isn’t just useful for predictive texting or smartphone voice recognition. Machine learning is constantly being applied to new industries and new problems. Whether you’re a marketer, video game designer, or programmer, Udemy has a course to help you apply machine learning to your work.
			</p>
			<h5>Price:</h5>
			<p>PhP: 30,000</p>
			<Button variant='primary'>Enroll</Button>
		</Card.Body>
	</Card>

	)
}